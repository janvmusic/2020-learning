## JS Grammar
Condensed knowledge for Javascript

## Table of Contents
- [Chapter 3 - Welcome to Javascript](https://github.com/janvmusic/2020-learning/blob/master/js-grammar/chapter3.md)
- [Chapter 4 - Statements](https://github.com/janvmusic/2020-learning/blob/master/js-grammar/chapter4.md)
- [Chapter 5 - Primitive types](https://github.com/janvmusic/2020-learning/blob/master/js-grammar/chapter5.md)
- [Chapter 6 - Type coercion madness](https://github.com/janvmusic/2020-learning/blob/master/js-grammar/chapter6.md)
- [Chapter 7 - Variables](https://github.com/janvmusic/2020-learning/blob/master/js-grammar/chapter7.md)
- [Chapter 8 - Arithmetic](https://github.com/janvmusic/2020-learning/blob/master/js-grammar/chapter8.md)
- [Chapter 9 - ...rest and ...spread](https://github.com/janvmusic/2020-learning/blob/master/js-grammar/chapter9.md)
- [Chapter 10 - Closures](https://github.com/janvmusic/2020-learning/blob/master/js-grammar/chapter10.md)
- [Chapter 11 - Loops](https://github.com/janvmusic/2020-learning/blob/master/js-grammar/chapter11.md)
- [Chapter 12 - Arrays](https://github.com/janvmusic/2020-learning/blob/master/js-grammar/chapter12.md)
- [Chapter 13 - Function](https://github.com/janvmusic/2020-learning/blob/master/js-grammar/chapter13.md)
- [Chapter 14 - High-order Functions](https://github.com/janvmusic/2020-learning/blob/master/js-grammar/chapter14.md)
- [Chapter 15 - Arrow Functions](https://github.com/janvmusic/2020-learning/blob/master/js-grammar/chapter15.md)
- [Chapter 16 - Creating HTML elements dynamically](https://github.com/janvmusic/2020-learning/blob/master/js-grammar/chapter16.md)
- [Chapter 17 - Prototype](https://github.com/janvmusic/2020-learning/blob/master/js-grammar/chapter17.md)
- [Chapter 18 - Object Oriented Programming](https://github.com/janvmusic/2020-learning/blob/master/js-grammar/chapter18.md)
- [Chapter 19 - Event loop](https://github.com/janvmusic/2020-learning/blob/master/js-grammar/chapter19.md)
- [Chapter 20 - Call Stack](https://github.com/janvmusic/2020-learning/blob/master/js-grammar/chapter20.md)

## Notes & Concepts
- Hoisting: default behavior of moving all the declarations at the top of the scope before code execution. Functions and variables are moved to the top before it's execution
- Hosting: assigning values to variables that don't exist, creates a variable in global scope.
- Marshalling: convert objects from memory to a format that can be written to disk
- `var` can cause hoisting bugs, prefer to use `let` or `const`
- **High-order functions**: because they take another function as argument or return a function
- **Rendering** is the act of displaying something in the screen
- A **class** is an abstract representation of an object
- Prototype Inheritance: Create between links and parents objects
