## Practical Object Oriented Design

## Table of Contents
- [Chapter 1 - In Praise of Design](https://github.com/janvmusic/2020-learning/blob/master/practical-ood/chapter1.md)
- [Chapter 2 - Designing Classes with a Single Responsibility](https://github.com/janvmusic/2020-learning/blob/master/practical-ood/chapter2.md)
- [Chapter 3 - Managing Dependencies](https://github.com/janvmusic/2020-learning/blob/master/practical-ood/chapter3.md)
- [Chapter 4 - Creating flexible interfaces](https://github.com/janvmusic/2020-learning/blob/master/practical-ood/chapter4.md)
- [Chapter 5 - Reducing costs with Duck Typing](https://github.com/janvmusic/2020-learning/blob/master/practical-ood/chapter5.md)
- [Chapter 6 - Acquiring behavior through inheritance](https://github.com/janvmusic/2020-learning/blob/master/practical-ood/chapter6.md)
- [Chapter 7 - Sharing role behavior with Modules](https://github.com/janvmusic/2020-learning/blob/master/practical-ood/chapter7.md)
- [Chapter 8 - Combining objects with Composition](https://github.com/janvmusic/2020-learning/blob/master/practical-ood/chapter8.md)

## Notes & Concepts
- How do you define _dependency inversion_?
  - Trust in abstract interfaces instead of implementations
- Why Java decided to go with static typing?
  -